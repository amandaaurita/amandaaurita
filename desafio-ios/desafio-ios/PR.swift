//
//  PR.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/26/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import Gloss

public struct PR: Decodable{
    let title: String?
    let body: String?
    let created_at: NSDate?
    let user: Dev?
    
    public init?(json: JSON){
        self.title = "title" <~~ json
        self.body = "body" <~~ json
        self.created_at = "created_at" <~~ json
        self.user = "user" <~~ json
    }

}
