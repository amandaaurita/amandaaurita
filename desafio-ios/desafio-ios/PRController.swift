//
//  PRController.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/26/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import UIKit

class PRController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navItem: UINavigationItem!
    
    
    let dao = DAO.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self

        self.navItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        //Get All PR
        dao.getAllPR(tbl: self.tableView)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dao.getPRs().count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PRCell", for: indexPath as IndexPath) as! PRCell
        cell.lblTitle.text = dao.getPRs()[indexPath.row].title
        cell.lblBody.text = dao.getPRs()[indexPath.row].body
        cell.lblUsername.text = dao.getPRs()[indexPath.row].user?.login
        cell.lblNameUser.text = String(describing: dao.getPRs()[indexPath.row].user!.id!)
        cell.lblCreatedAt.text = String(describing: dao.getPRs()[indexPath.row].created_at)
        
        let imageUrlString = dao.getPRs()[indexPath.row].user!.avatar_url as String!
        let imageUrl:URL = URL(string: imageUrlString!)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        cell.imageView?.image = image
        
        return cell
    }

}
