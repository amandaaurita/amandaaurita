//
//  RepoCell.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/26/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import UIKit

class RepoCell: UITableViewCell {
    
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblForks: UILabel!
    @IBOutlet weak var lblStars: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblNameUser: UILabel!
    @IBOutlet weak var avatar: UIImage!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
