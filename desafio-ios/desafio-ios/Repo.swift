//
//  Repo.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/25/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import Gloss

public struct Repo: Decodable {
    var id: Int?
    var name: String?
    var full_name:String?
    var description: String?
    var watchers_count: Int?
    var forks_count: Int?
    var owner: Dev?
    
    public init?(json: JSON) {
        self.id = "id" <~~ json
        self.name = "name" <~~ json
        self.full_name = "full_name" <~~ json
        self.description = "description" <~~ json
        self.watchers_count = "watchers_count" <~~ json
        self.forks_count = "forks_count" <~~ json
        self.owner = "owner" <~~ json
    }
    
    


}
