//
//  Dev.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/26/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import Foundation
import Gloss

public struct Dev: Decodable {
    let login: String?
    let id: Int?
    let avatar_url: String?
    
    public init?(json: JSON) {
       self.login = "login" <~~ json
        self.id = "id" <~~ json
        self.avatar_url = "avatar_url" <~~ json
    }
    
}
