//
//  DAO.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/26/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//


import Foundation
import Alamofire
import Gloss

class DAO: NSObject {
    
    //Variables
    var repositories = [Repo]()    // Represent all the Java repositories
    var prs = [PR]()                // Represent all pull request from select repository
    var jsonData: Dictionary<String, Any> = [:]
    var currentCreator: String = ""
    var currentRepo: String = ""

    
    
    //Singleton
    class var sharedInstance: DAO {
        struct Singleton {
            static var Instance: DAO!
        }
        
        if (Singleton.Instance == nil) {
            Singleton.Instance = DAO()
        }
        
        return Singleton.Instance
    }
    
    private override init () {
        
    }
    
    func setRepo(r: Repo){
        repositories.append(r);
    }
    
    func getRepo()->[Repo]{
        return repositories
    }
    func setPRs(pr: PR){
        prs.append(pr)
    }
    func getPRs()-> [PR]{
        return prs
    }
    func setCurrentCreator(creator: String){
        self.currentCreator = creator
    }

    func getCurrentCreator() -> String{
        return self.currentCreator
    }
    
    func setCurrentRepo(repo: String){
        self.currentRepo = repo
    }
    
    func getCurrentRepo() -> String{
        return self.currentRepo
    }

    //Data Acess Functions
    
    func createAllRepo(tbl: UITableView){


        Alamofire.request("https://api.github.com/search/repositories?q=language:Java&sort=stars&page=1").responseJSON() { response in
            
            if let JSON = response.result.value {
                
                    self.jsonData = (JSON as? Dictionary<String, AnyObject>)!
                    var rp: Repo
                    
                    for repos in self.jsonData["items"] as! NSArray{
                        rp = Repo.init(json: repos as! JSON)!
                        self.setRepo(r: rp)
                    }
                tbl.reloadData()
            }
        }
    }
    
    
    func getAllPR(tbl: UITableView){
        
        let url = "https://api.github.com/repos/\(self.getCurrentCreator())/\(self.getCurrentRepo())/pulls"
        Alamofire.request(url).responseJSON() { response in
            
                var pr: PR
                for item in response.result.value! as! NSArray{
                    pr = PR.init(json: item as! JSON)!
                    self.setPRs(pr: pr)
                }
                tbl.reloadData()
            }
    }
}

