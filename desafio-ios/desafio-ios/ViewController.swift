//
//  ViewController.swift
//  desafio-ios
//
//  Created by Amanda Aurita Araujo Fernandes on 11/25/16.
//  Copyright © 2016 Amanda Aurita Araujo Fernandes. All rights reserved.
//

import UIKit
import Alamofire
import Gloss



class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    var dao : DAO!
    var jsonData: Dictionary<String, Any> = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        dao = DAO.sharedInstance
        dao.createAllRepo(tbl: self.tableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dao.getRepo().count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TextCell", for: indexPath as IndexPath) as! RepoCell
        
        cell.lblName.text = dao.getRepo()[indexPath.row].name
        cell.lblDesc.text = dao.getRepo()[indexPath.row].description
        cell.lblForks.text = "Forks: \(String(describing: dao.getRepo()[indexPath.row].forks_count!))"
        cell.lblStars.text = "Stars: \(String(describing: dao.getRepo()[indexPath.row].watchers_count!))"
        cell.lblUsername.text = dao.getRepo()[indexPath.row].owner?.login!
        cell.lblNameUser.text = "\(String(describing: dao.getRepo()[indexPath.row].owner!.id!))"
        
        let imageUrlString = dao.getRepo()[indexPath.row].owner!.avatar_url as String!
        let imageUrl:URL = URL(string: imageUrlString!)!
        let imageData:NSData = NSData(contentsOf: imageUrl)!
        let image = UIImage(data: imageData as Data)
        cell.imageView?.image = image
        
        
          return cell
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        //Set Current Repository
        dao.setCurrentRepo(repo: dao.getRepo()[indexPath.row].name!)
        dao.setCurrentCreator(creator: (dao.getRepo()[indexPath.row].owner?.login!)!)
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

        // Dispose of any resources that can be recreated.
    }


}

